# Opositatest Prueba

La idea de esta prueba es realizar una página html, con un listado de items. En este caso oposiciones.

Estos items vendrán devueltos a través de un api, que devolverá los datos al frontend. Para la prueba se puede obviar esta llamada o simular una llamada, pero en un lugar de responderla un servidor recogerla directamente de un json estático.

La idea es que al seleccionar las diferentes categorías del lateral izquierdo el listado de items de la derecha se vaya actualizando.

Cada item, tendrá valores para diferentes categorías y puede estar incluida en varios. Por ejemplo la oposición Jueces y Fiscales puede estar en la categoria 1 y en la categoria 2, además será de ámbito autonómico y tendrá un valor salarial de 2000€.

Para la prueba se puede usar cualquier framework javascript ( vue, Angular o React ) lo importante de la prueba no es el diseño, lo importante es como está organizado el javascript, para su buen mantenimiento, comprensión de otros compañeros y su reutilización para otras páginas que puedan servirse de este tipo de componentes.

## Develop / Test / Deploy

### Local
```
$ yarn start
```

### Test
```
$ yarn test
```

### Deploy
```
$ yarn build
```