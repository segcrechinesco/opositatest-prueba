import React, { useState, useEffect } from 'react';
import { xor, uniq, flatten, map, sortBy, last } from 'lodash';
import './App.css';
import { Checkbox, Radio, RangeSlider } from './components';
import { Categories, Scopes, Studies, Plans } from './services';

const App = () => {
  let [filters, setFilters] = useState({
    categories: [],
    scopes: [],
    studies: 'any',
    minSalary: null,
    maxSalary: null,
  });

  let [categories, setCategories] = useState([]);
  let [isLoadingCategories, setIsLoadingCategories] = useState(true);

  let [scopes, setScopes] = useState([]);
  let [isLoadingScopes, setIsLoadingScopes] = useState(true);

  let [studies, setStudies] = useState([]);
  let [isLoadingStudies, setIsLoadingStudies] = useState(true);

  let [plans, setPlans] = useState([]);
  let [isLoadingPlans, setIsLoadingPlans] = useState(true);

  let isLoadingFilters = isLoadingCategories && isLoadingScopes && isLoadingStudies;

  let planCategoryIds = uniq(flatten(map(plans, 'categories')));

  let planCategories = categories.filter((category) => {
    return planCategoryIds.includes(category.id);
  });

  let planSalaries = sortBy(map(plans, 'approximate_salary'));

  let minSalary = planSalaries[0];
  let maxSalary = last(planSalaries);

  const fetchCategories = () => {
    setIsLoadingCategories(true);

    // EXPLANATION: By default we return with the mock data all the available categories
    // but we should specify to the service that we want all the items with a parameter like "per_page=-1"
    Categories.search()
      .then((response) => {
        setIsLoadingCategories(false);
        setCategories(response.data.items);
      })
      .catch((error) => {
        setIsLoadingCategories(false);
      });
  };

  const fetchScopes = () => {
    setIsLoadingScopes(true);

    Scopes.search()
      .then((response) => {
        setIsLoadingScopes(false);
        setScopes(response.data.items);
      })
      .catch((error) => {
        setIsLoadingScopes(false);
      });
  };

  const fetchStudies = () => {
    setIsLoadingStudies(true);

    Studies.search()
      .then((response) => {
        setIsLoadingStudies(false);
        setStudies(response.data.items);
      })
      .catch((error) => {
        setIsLoadingStudies(false);
      });
  };

  const handleChangeCategories = (categoryId) => {
    setFilters({
      ...filters, 
      categories: xor(filters.categories, [categoryId]),
    });
  };

  const handleChangeScopes = (scopeId) => {
    setFilters({
      ...filters, 
      scopes: xor(filters.scopes, [scopeId]),
    });
  };

  const handleChangeStudies = (studyId) => {
    setFilters({
      ...filters, 
      studies: studyId,
    });
  };

  const handleChangeRangeSalary = (range) => {
    setFilters({
      ...filters, 
      minSalary: range.min,
      maxSalary: range.max,
    });
  };

  useEffect(() => {
    fetchCategories();
    fetchScopes();
    fetchStudies();
  }, []);

  useEffect(() => {
    const fetchPlans = () => {
      setIsLoadingPlans(true);
  
      Plans.search()
        .then((response) => {
          setIsLoadingPlans(false);
  
          // TODO: Move this filters to the backend logic
          let plans = response.data.items.filter((plan) => {
            let valid = true;
  
            if(filters.categories.length > 0) {
              valid = false;
  
              plan.categories.forEach((id) => {
                if(filters.categories.includes(id)) {
                  valid = true;
                  return;
                }
              });
            }
  
            if(filters.scopes.length > 0) {
              valid &= filters.scopes.includes(plan.scope);
            }
  
            if(filters.studies && filters.studies !== 'any') {
              valid &= plan.minimum_studies === filters.studies
            }
  
            if(filters.minSalary) {
              valid &= plan.approximate_salary >= filters.minSalary;
            }
  
            if(filters.maxSalary) {
              valid &= plan.approximate_salary <= filters.maxSalary;
            }
  
            return valid;
          });
  
          setPlans(plans);
        })
        .catch((error) => {
          setIsLoadingPlans(false);
        });
    };

    fetchPlans();
  }, [filters]);
  
  let renderedPlans = planCategories.map((category) => {
    return (
      <div className="bg-white p-4 mb-4" key={category.id}>
        <h3>
          <div className="w-4 h-4 rounded inline-block" style={{backgroundColor: category.color}}></div>
          <span>{category.name}</span>
        </h3>
        <ul>
          {
            plans.filter((plan) => {
              return plan.categories.includes(category.id);
            }).map((plan) => 
              <li className="flex p-2 mb-2" key={plan.id}>
                <div className="w-12 h-12 flex rounded-full flex-shrink-0 flex-grow-0 justify-center items-center mr-4" style={{backgroundColor: category.color}}>
                  <span>icon</span>
                </div>
                <div>
                  <h4>{plan.name}</h4>
                  <p className="text-gray-600">Desde {plan.price} €/mes</p>
                </div>
              </li>
            )
          }
        </ul>
      </div>
    );
  });

  return(
    <div className="App">
      <div className="container mx-auto p-4">
        <h1 className="text-4xl font-bold">Planes y precios</h1>
        <div className="flex">
          <div className="w-1/3 p-4 mr-4 bg-gray-300">
            <div className="mb-2">
              <h3 className="font-bold mb-2">Categorías</h3>
              <ul>
                {
                  isLoadingFilters ? (
                    <p>Skeleton loading</p>
                  ) : (
                    categories.map((category) => 
                      <li key={category.id}>
                        <Checkbox value={category.id} checked={filters.categories.includes(category.id)} onChange={handleChangeCategories} />
                        <span>{category.name}</span>
                      </li>
                    )
                  )
                }
              </ul>
            </div>
            <div className="mb-2">
              <h3 className="font-bold mb-2">Ámbito</h3>
              <ul>
                {
                  isLoadingFilters ? (
                    <p>Skeleton loading</p>
                  ) : (
                    scopes.map((scope) => 
                      <li key={scope.id}>
                        <Checkbox value={scope.id} onChange={handleChangeScopes} />
                        <span>{scope.name}</span>
                      </li>
                    )
                  )
                }
              </ul>
            </div>
            <div className="mb-2">
              <h3 className="font-bold mb-2">Titulación</h3>
              <ul>
                {
                  isLoadingFilters ? (
                    <p>Skeleton loading</p>
                  ) : (
                    [<li key="any">
                      <Radio value="any" checked={filters.studies === 'any'} onChange={handleChangeStudies} />
                      <span>Cualquiera</span>
                    </li>,
                    studies.map((study) => {
                      return (
                        <li key={study.id}>
                          <Radio value={study.id} checked={filters.studies === study.id} onChange={handleChangeStudies} />
                          <span>{study.name}</span>
                        </li>
                      );
                    })]
                  )
                }
              </ul>
            </div>
            <div className="mb-2">
              <h3 className="font-bold mb-2">Rango salarial</h3>
              {
                isLoadingFilters ? (
                  <p>Skeleton loading</p>
                ) : (
                  <RangeSlider min={minSalary} max={maxSalary} onChange={handleChangeRangeSalary} />
                )
              }
            </div>
          </div>
          <div className="w-2/3">
            {
              isLoadingPlans ? (
                <p>Skeleton loading</p>
              ) : (
                renderedPlans.length > 0 ? (
                  renderedPlans
                ) : (
                  <p className="bg-white p-4 text-center">No se han encontrado planes con las características indicadas.</p>
                )
              )
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
