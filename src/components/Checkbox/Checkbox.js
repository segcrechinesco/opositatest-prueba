import React from 'react';
import { useToggle } from './../../hooks';
import classNames from 'classnames';
import './Checkbox.css';

const Checkbox = ({value, checked = false, onChange}) => {
  let [isChecked, toggleIsChecked] = useToggle(checked);

  const handleChange = () => {
    toggleIsChecked();
    typeof onChange === 'function' && onChange(value);
  };

  const rootClassNames = classNames({
    'Checkbox rounded bg-white inline-block': true,
    'Checkbox-checked': isChecked,
  });

  return (
    <div className={rootClassNames} onClick={handleChange}>
      <input type="checkbox" value={value} checked={isChecked} readOnly />
    </div>
  );
};

export default Checkbox;
