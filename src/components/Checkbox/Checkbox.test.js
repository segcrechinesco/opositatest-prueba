import React from 'react';
import { shallow, mount } from 'enzyme';
import Checkbox from './Checkbox';

test('should render', () => {
  const wrapper = shallow(<Checkbox />);
  expect(wrapper).toMatchSnapshot();
});

describe('should toggle state if clicked', () => {
  const wrapper = shallow(<Checkbox />);

  test('should be show checked if not checked', () => {
    wrapper.find('.Checkbox').simulate('click');
    expect(wrapper.find('.Checkbox-checked')).toHaveLength(1);
  });

  test('should be show unchecked if checked', () => {
    wrapper.find('.Checkbox').simulate('click');
    expect(wrapper.find('.Checkbox-checked')).toHaveLength(0);
  });
});

test('should fire change event if clicked', () => {
  const onChange = jest.fn();
  const wrapper = mount(<Checkbox onChange={onChange} />);

  wrapper.find('.Checkbox').simulate('click');
  expect(onChange).toHaveBeenCalled();
});

test('should set value', () => {
  const wrapper = shallow(<Checkbox value="test" />);
  expect(wrapper.find('input[type="checkbox"]').props()).toHaveProperty('value', 'test');
});

test('should set is checked', () => {
  const wrapper = shallow(<Checkbox checked={true} />);
  expect(wrapper.find('input[type="checkbox"]').props()).toHaveProperty('checked', true);
});

