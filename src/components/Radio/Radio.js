import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import './Radio.css';

const Radio = ({value, checked = false, onChange}) => {
  let [isChecked, setIsChecked] = useState(checked);

  const handleChange = () => {
    setIsChecked(true);
    typeof onChange === 'function' && onChange(value);
  };

  useEffect(() => {
    setIsChecked(checked);
  }, [checked]);

  const rootClassNames = classNames({
    'Radio rounded-full bg-white inline-block': true,
    'Radio-checked': isChecked,
  });

  return (
    <div className={rootClassNames} onClick={handleChange}>
      <input type="radio" value={value} checked={isChecked} readOnly />
    </div>
  );
};

export default Radio;
