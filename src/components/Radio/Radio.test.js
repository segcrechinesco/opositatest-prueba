import React from 'react';
import { shallow, mount } from 'enzyme';
import Radio from './Radio';

test('should render', () => {
  const wrapper = shallow(<Radio />);
  expect(wrapper).toMatchSnapshot();
});

describe('should toggle state if clicked', () => {
  const wrapper = shallow(<Radio />);

  test('should be show checked if not checked', () => {
    wrapper.find('.Radio').simulate('click');
    expect(wrapper.find('.Radio-checked')).toHaveLength(1);
  });

  test('should be show checked if checked', () => {
    wrapper.find('.Radio-checked').simulate('click');
    expect(wrapper.find('.Radio-checked')).toHaveLength(1);
  });
});

test('should fire change event if clicked', () => {
  const onChange = jest.fn();
  const wrapper = mount(<Radio onChange={onChange} />);

  wrapper.find('.Radio').simulate('click');
  expect(onChange).toHaveBeenCalled();
});

test('should set value', () => {
  const wrapper = shallow(<Radio value="test" />);
  expect(wrapper.find('input[type="radio"]').props()).toHaveProperty('value', 'test');
});

test('should set is checked', () => {
  const wrapper = shallow(<Radio checked={true} />);
  expect(wrapper.find('input[type="radio"]').props()).toHaveProperty('checked', true);
});

