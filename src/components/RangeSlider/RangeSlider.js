import React, { useState, useEffect, useRef } from 'react';
import './RangeSlider.css';

const RangeSlider = ({min = 0, max = 0, onChange, }) => {
  let [values, setValues] = useState({
    min: min,
    max: max,
  });

  const minInput = useRef();
  const maxInput = useRef();

  const handleChangeMin = () => {
    const newValues = {...values, min: minInput.current.value, };
    setValues(newValues);
    typeof onChange === 'function' && onChange(newValues);
  };

  const handleChangeMax = () => {
    const newValues = {...values, max: maxInput.current.value, };
    setValues(newValues);
    typeof onChange === 'function' && onChange(newValues);
  };

  useEffect(() => {
    minInput.current.value = min;
    maxInput.current.value = max;
  }, [min, max]);

  return (
    <div className="RangeSlider flex">
      <input ref={minInput} className="w-1/2 mr-2 p-2" type="number" min={min} max={values.max} onBlur={handleChangeMin} />
      <input ref={maxInput} className="w-1/2 p-2" type="number" min={values.min} max={max} onBlur={handleChangeMax} />
    </div>
  );
};

export default RangeSlider;
