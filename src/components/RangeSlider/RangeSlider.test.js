import React from 'react';
import { shallow } from 'enzyme';
import RangeSlider from './RangeSlider';

test('should render', () => {
  const wrapper = shallow(<RangeSlider />);
  expect(wrapper).toMatchSnapshot();
});

test('should set value', () => {
  const wrapper = shallow(<RangeSlider value="test" />);
  expect(true).toEqual(true);
});

