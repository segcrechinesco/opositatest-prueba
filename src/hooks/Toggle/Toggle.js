import { useState, useCallback } from 'react';

const Toggle = (defaultValue) => {
  const [toggleValue, setToggleValue] = useState(defaultValue);

  const toggler = useCallback(() => setToggleValue(!toggleValue), [toggleValue]);

  return [toggleValue, toggler];
};

export default Toggle;