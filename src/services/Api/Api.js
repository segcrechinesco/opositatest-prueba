import axios from 'axios';

const Api = axios.create({
  baseURL: `${window.location.origin}/api`,
});

export default Api;

export const DEFAULT_DELAY_TIME = 500;