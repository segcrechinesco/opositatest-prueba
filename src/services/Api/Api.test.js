import Api from './Api';

test('should create axios instance', () => {
  expect(Api).toHaveProperty('defaults.baseURL');
});