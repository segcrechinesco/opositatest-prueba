import Api, { DEFAULT_DELAY_TIME } from './../Api';

const Categories = {
  // TODO: backend should return categories filtered by specified attributes
  // but we move this temporally to the frontend
  search: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        Api.get(`/categories.json`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      }, DEFAULT_DELAY_TIME);
    });
  },
};

export default Categories;