import Api, { DEFAULT_DELAY_TIME } from './../Api';

const Plans = {
  // TODO: backend should return plans filtered by specified attributes to:
  // - order items by bussiness interests
  // - prevents duplicated filtering code between apps
  // By now we move this temporally to the frontend
  search: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        Api.get(`/plans.json`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      }, DEFAULT_DELAY_TIME);
    });
  },
};

export default Plans;