import Api, { DEFAULT_DELAY_TIME } from './../Api';

const Scopes = {
  // TODO: backend should return scopes filtered by specified attributes
  // but we move this temporally to the frontend
  search: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        Api.get(`/scopes.json`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      }, DEFAULT_DELAY_TIME);
    });
  },
};

export default Scopes;