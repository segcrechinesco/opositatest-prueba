import Api, { DEFAULT_DELAY_TIME } from './../Api';

const Studies = {
  // TODO: backend should return studies filtered by specified attributes
  // but we move this temporally to the frontend
  search: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        Api.get(`/studies.json`)
          .then((response) => resolve(response))
          .catch((error) => reject(error));
      }, DEFAULT_DELAY_TIME);
    });
  },
};

export default Studies;